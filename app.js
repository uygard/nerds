// Require Necessary Modules

const express = require("express");
const bodyParser = require("body-parser");
const bcrpypt = require("bcrypt")
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const flash = require("connect-flash")
const passport = require("passport");
const session = require("express-session");
const User = require("./models/user");
const Post = require("./models/post");
const Postf = require("./models/postf");
const user = require("./models/user");
require("./config/localStrategy")



const app = express();
app.use(express.static('public'))

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser("passport"));

app.use(
  session({
    cookie: {
      maxAge: 60000
    },
    resave: true,
    secret: "passport",
    saveUninitialized: true
  })
);
app.use(flash())

app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  res.locals.user = req.user
  next();
});



app.set("view engine", "ejs");

mongoose.connect("mongodb+srv://uygar:uygar@cluster0.n1rez.mongodb.net/isbulDB", {
  useNewUrlParser: true
});



app.get("/", (req, res, next) => {
  res.render("index")
});




// LOGIN LOGOUT REGISTER

app.get("/login", (req, res, next) => {
  res.render("login")
})

app.post("/login", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
  })(req, res, next)
})

app.get("/register", (req, res, next) => {
  if (req.user) {
    res.redirect("/")
  } else
    res.render("register");
})


app.post("/register", (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const name = req.body.name
  const lastName = req.body.lastName

  // email Verification
  User.findOne({
      email: email
    })
    .then(user => {
      if (user) {

        return res.render("register");
      } else {
        bcrpypt.genSalt(10, (err, salt) => {
          bcrpypt.hash(password, salt, (err, hash) => {
            if (err) throw err;
            const newUser = new User({
              email: email,
              password: hash,
              name: name,
              lastName: lastName
            });

            newUser
              .save()
              .then(user => {


                res.redirect("/");
              })
              .catch(err => console.log(err));
          });
        });
      }
    })
    .catch(err => console.log(err));
  req.login(req.user, function(err) {
    if (!err) {
      res.redirect('/');
    } else {
      //handle error
    }
  })
})
app.get("/logout", (req, res, next) => {

  if (req.user) {
    req.logout();
    res.redirect("/login")
  } else {
    res.render("/")
  }
})


// POST DUZENLE
app.get("/:postId/duzenle", (req, res, next) => {
  const postId = req.params.postId; Post.findById(postId, function(err, foundPost) { res.render("duzenle", {post: foundPost})})
})

app.post("/:postId/duzenle", function(req, res) {
	const postId = req.params.postId
	Post.findById(postId, function(err, foundPost) {foundPost.title = req.body.title; foundPost.content = req.body.content; foundPost.save() })

  res.redirect("/posts/" + postId)
})









// POST DELETE DENEME

app.post("/:postId/delete", function(req, res) {
	const postId = req.params.postId
	Post.findByIdAndDelete(postId, function (err, docs) {
    if (err){
        console.log(err)
    }
    else{
        console.log("Deleted : ", docs);
    }
});

	res.render("index")

})

// USER DELETE DENEME

app.post("/:userId/udelete", function(req, res) {
	const userId = req.user.id
	User.findByIdAndDelete(userId, function (err, docs) {
    if (err){
        console.log(err)
    }
    else{
        console.log("Deleted : ", docs);
    }
});
req.logout();
res.redirect("/")

})

app.get("/userDelete", (req, res, next) => {
  res.render("userDelete")
})


// SIFREMI UNUTTUM

app.get("/sifre", (req, res, next) => {
  res.render("sifre")
})

app.post("/sifre", function(req, res) {

  const email = req.body.email;
  const password = req.body.password;


  User.findOne({
    email: email
  })
  .then(user => {
    if (!user) {

      return res.render("register");
    } else {
      bcrpypt.genSalt(10, (err, salt) => {
        bcrpypt.hash(password, salt, (err, hash) => {
          if (err) throw err;
           user.password = hash
  user.save()
        });
      });
    }
  })

res.render("login")
})


// Freelancer listesi

app.get("/postsf", function(req, res) {

  Postf.find({}, function(err, posts) {
    res.render("postsf", {
      posts: posts
    });
  });
});


// FREELANCER İLANI

app.get("/createPostf", function(req, res) {

  if (req.user) {
    res.render("createPostf")
  } else {
    res.render("login")
  }
})

app.post("/createPostf", function(req, res) {

  const id = req.user.id
  User.findById(id, function(err, foundUser) {

    const newPost = new Postf({
      title: req.body.postTitle,
      content: req.body.postContent,
      postedBy: id
    });


    newPost.save(function(err) {
      if (err)
        console.log(err);
    });

    foundUser.postsf.push(newPost)
    foundUser.save(function(err) {
      if (err) {
        console.log(err)
      }
      res.redirect('/')
    })
  })
})



// POST
app.get("/createPost", function(req, res) {

  if (req.user) {
    res.render("createPost")
  } else {
    res.render("login")
  }
})

app.post("/createPost", function(req, res) {

  const id = req.user.id
  User.findById(id, function(err, foundUser) {

    const newPost = new Post({
      title: req.body.postTitle,
      content: req.body.postContent,
      postedBy: id
    });


    newPost.save(function(err) {
      if (err)
        console.log(err);
    });

    foundUser.posts.push(newPost)
    foundUser.save(function(err) {
      if (err) {
        console.log(err)
      }
      res.redirect('/')
    })
  })
})


app.get("/posts", function(req, res) {

  Post.find({}, function(err, posts) {
    res.render("posts", {
      posts: posts
    });
  });
});

app.get("/myposts", function(req, res) {

  if (req.user) {
    const id = req.user.id


    Post.find({
      postedBy: id
    }, function(err, post) {
      res.render("myposts", {
        posts: post
      })
    })
  } else {
    res.redirect("/login")
  }
})


app.get("/posts/:postId", function(req, res) {

  const postId = req.params.postId;

  if (req.user) {
    const userId = req.user.id

    Post.findById(postId, function(err, foundPost) {
      const applied = foundPost.applicants.includes(userId)
      res.render("post", {
        applied: applied,
        post: foundPost
      })
    })
  }
  else {
    Post.findById(postId, function(err, foundPost) {
      res.render("post", {
        post: foundPost
      })
    })
  }

});

app.post("/:postId/apply", function(req, res) {
  if (req.user) {
    const postId = req.params.postId
    const userId = req.user.id

    Post.findById(postId, function(err, foundPost) {
      foundPost.applicants.push(userId)
      foundPost.save()
    })
    res.redirect(req.get('referer'))
  } else {

    res.render("login")

  }

})

app.post("/:postId/basvuruIptal", function(req, res) {
  if (req.user) {
    const postId = req.params.postId
    const userId = req.user.id

    Post.findById(postId, function(err, foundPost) {
      const applied = foundPost.applicants.includes(userId)

      const index = foundPost.applicants.indexOf(userId)
      foundPost.applicants.splice(index, 1)
      foundPost.save()

      res.redirect(req.get('referer'))
    })

  } else {
    res.render("login")
  }
})

app.get("/:postId/applicants", function(req, res) {

  const postId = req.params.postId
  Post.findById(postId).populate("applicants").exec(function(err, foundPost) {
    res.render("applicants", {
      applicants: foundPost.applicants
    })
  })

})

app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
