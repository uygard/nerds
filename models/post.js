const mongoose = require("mongoose")
const User = require("./user")
const postSchema =  mongoose.Schema ({

  postedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  title: String,
  content: String,
  applicants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
})

module.exports = mongoose.model("Post", postSchema)
