const mongoose = require("mongoose")
const Post = require("./post")
const Postf = require("./postf")
const userSchema =  new mongoose.Schema ({

  name: String,
  lastName: String,
  password: {type: String, required:true },
  email : {
    type: String,
    required: true,

  },
  age: Number,

  posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
  postsf: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Postf' }],


})
module.exports = mongoose.model('User', userSchema);
